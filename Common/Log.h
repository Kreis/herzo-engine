#ifndef _LOG_H_
#define _LOG_H_

#include <iostream>
#include <fstream>
#include <memory>
#include <assert.h>
#include <cstring>

namespace Herzo {
class Log {
public:
  virtual ~Log();
  void write(const std::string& message);
  void close();

  static Log* getSingleton();

private:
  Log();

  static std::unique_ptr<Log> singleton;
  std::ofstream file;
};
}

#ifdef DEBUG
  #define LOG(v) Herzo::Log::getSingleton()->write( \
    std::string(__FILE__) + " " + \
    std::to_string(__LINE__) + " " + \
    v)
  #define PANIC Herzo::Log::getSingleton()->close(); \
    assert(false);
#else
  #define LOG(v)
  #define PANIC
#endif

#endif
