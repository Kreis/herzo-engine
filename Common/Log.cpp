#include "Log.h"

namespace Herzo {

std::unique_ptr<Log> Log::singleton = nullptr;

Log::Log() {
  this->file.open("log", std::ios::out);
  if ( ! this->file.is_open()) {
    assert(false);
  }
}

Log::~Log() {
  this->close();
}


void Log::write(const std::string& message) {
  this->file << message << '\n';
}

void Log::close() {
  if (this->file.is_open()) {
    this->file.close();
  }
}

Log* Log::getSingleton() {
  if (Log::singleton == nullptr) {
    Log::singleton = std::unique_ptr<Log>(new Log());
  }
  return Log::singleton.get();
}
}
