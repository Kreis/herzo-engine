#include "InputDataType.h"

namespace Herzo {
InputDataType::InputDataType() {
}

sf::Keyboard::Key InputDataType::getSfmlKey(Key key) {
  if (key == Key::A) {
    return sf::Keyboard::Key::A;
  } else if (key == Key::B) {
    return sf::Keyboard::Key::B;
  } else if (key == Key::C) {
    return sf::Keyboard::Key::C;
  } else if (key == Key::D) {
    return sf::Keyboard::Key::D;
  } else if (key == Key::E) {
    return sf::Keyboard::Key::E;
  } else if (key == Key::F) {
    return sf::Keyboard::Key::F;
  } else if (key == Key::G) {
    return sf::Keyboard::Key::G;
  } else if (key == Key::H) {
    return sf::Keyboard::Key::H;
  } else if (key == Key::I) {
    return sf::Keyboard::Key::I;
  } else if (key == Key::J) {
    return sf::Keyboard::Key::J;
  } else if (key == Key::K) {
    return sf::Keyboard::Key::K;
  } else if (key == Key::L) {
    return sf::Keyboard::Key::L;
  } else if (key == Key::M) {
    return sf::Keyboard::Key::M;
  } else if (key == Key::N) {
    return sf::Keyboard::Key::N;
  } else if (key == Key::O) {
    return sf::Keyboard::Key::O;
  } else if (key == Key::P) {
    return sf::Keyboard::Key::P;
  } else if (key == Key::Q) {
    return sf::Keyboard::Key::Q;
  } else if (key == Key::R) {
    return sf::Keyboard::Key::R;
  } else if (key == Key::S) {
    return sf::Keyboard::Key::S;
  } else if (key == Key::T) {
    return sf::Keyboard::Key::T;
  } else if (key == Key::U) {
    return sf::Keyboard::Key::U;
  } else if (key == Key::V) {
    return sf::Keyboard::Key::V;
  } else if (key == Key::W) {
    return sf::Keyboard::Key::W;
  } else if (key == Key::X) {
    return sf::Keyboard::Key::X;
  } else if (key == Key::Y) {
    return sf::Keyboard::Key::Y;
  } else if (key == Key::Z) {
    return sf::Keyboard::Key::Z;
  } else if (key == Key::Num0) {
    return sf::Keyboard::Key::Num0;
  } else if (key == Key::Num1) {
    return sf::Keyboard::Key::Num1;
  } else if (key == Key::Num2) {
    return sf::Keyboard::Key::Num2;
  } else if (key == Key::Num3) {
    return sf::Keyboard::Key::Num3;
  } else if (key == Key::Num4) {
    return sf::Keyboard::Key::Num4;
  } else if (key == Key::Num5) {
    return sf::Keyboard::Key::Num5;
  } else if (key == Key::Num6) {
    return sf::Keyboard::Key::Num6;
  } else if (key == Key::Num7) {
    return sf::Keyboard::Key::Num7;
  } else if (key == Key::Num8) {
    return sf::Keyboard::Key::Num8;
  } else if (key == Key::Num9) {
    return sf::Keyboard::Key::Num9;
  } else if (key == Key::Escape) {
    return sf::Keyboard::Key::Escape;
  } else if (key == Key::LControl) {
    return sf::Keyboard::Key::LControl;
  } else if (key == Key::LShift) {
    return sf::Keyboard::Key::LShift;
  } else if (key == Key::LAlt) {
    return sf::Keyboard::Key::LAlt;
  } else if (key == Key::LSystem) {
    return sf::Keyboard::Key::LSystem;
  } else if (key == Key::RControl) {
    return sf::Keyboard::Key::RControl;
  } else if (key == Key::RShift) {
    return sf::Keyboard::Key::RShift;
  } else if (key == Key::RAlt) {
    return sf::Keyboard::Key::RAlt;
  } else if (key == Key::RSystem) {
    return sf::Keyboard::Key::RSystem;
  } else if (key == Key::Menu) {
    return sf::Keyboard::Key::Menu;
  } else if (key == Key::LBracket) {
    return sf::Keyboard::Key::LBracket;
  } else if (key == Key::RBracket) {
    return sf::Keyboard::Key::RBracket;
  } else if (key == Key::Comma) {
    return sf::Keyboard::Key::Comma;
  } else if (key == Key::Period) {
    return sf::Keyboard::Key::Period;
  } else if (key == Key::Quote) {
    return sf::Keyboard::Key::Quote;
  } else if (key == Key::Slash) {
    return sf::Keyboard::Key::Slash;
  } else if (key == Key::Tilde) {
    return sf::Keyboard::Key::Tilde;
  } else if (key == Key::Equal) {
    return sf::Keyboard::Key::Equal;
  } else if (key == Key::Dash) {
    return sf::Keyboard::Key::Dash;
  } else if (key == Key::Space) {
    return sf::Keyboard::Key::Space;
  } else if (key == Key::Enter) {
    return sf::Keyboard::Key::Enter;
  } else if (key == Key::Tab) {
    return sf::Keyboard::Key::Tab;
  } else if (key == Key::PageUp) {
    return sf::Keyboard::Key::PageUp;
  } else if (key == Key::PageDown) {
    return sf::Keyboard::Key::PageDown;
  } else if (key == Key::End) {
    return sf::Keyboard::Key::End;
  } else if (key == Key::Home) {
    return sf::Keyboard::Key::Home;
  } else if (key == Key::Insert) {
    return sf::Keyboard::Key::Insert;
  } else if (key == Key::Delete) {
    return sf::Keyboard::Key::Delete;
  } else if (key == Key::Add) {
    return sf::Keyboard::Key::Add;
  } else if (key == Key::Subtract) {
    return sf::Keyboard::Key::Subtract;
  } else if (key == Key::Multiply) {
    return sf::Keyboard::Key::Multiply;
  } else if (key == Key::Divide) {
    return sf::Keyboard::Key::Divide;
  } else if (key == Key::Left) {
    return sf::Keyboard::Key::Left;
  } else if (key == Key::Right) {
    return sf::Keyboard::Key::Right;
  } else if (key == Key::Up) {
    return sf::Keyboard::Key::Up;
  } else if (key == Key::Down) {
    return sf::Keyboard::Key::Down;
  } else if (key == Key::Numpad0) {
    return sf::Keyboard::Key::Numpad0;
  } else if (key == Key::Numpad1) {
    return sf::Keyboard::Key::Numpad1;
  } else if (key == Key::Numpad2) {
    return sf::Keyboard::Key::Numpad2;
  } else if (key == Key::Numpad3) {
    return sf::Keyboard::Key::Numpad3;
  } else if (key == Key::Numpad4) {
    return sf::Keyboard::Key::Numpad4;
  } else if (key == Key::Numpad5) {
    return sf::Keyboard::Key::Numpad5;
  } else if (key == Key::Numpad6) {
    return sf::Keyboard::Key::Numpad6;
  } else if (key == Key::Numpad7) {
    return sf::Keyboard::Key::Numpad7;
  } else if (key == Key::Numpad8) {
    return sf::Keyboard::Key::Numpad8;
  } else if (key == Key::Numpad9) {
    return sf::Keyboard::Key::Numpad9;
  } else if (key == Key::F1) {
    return sf::Keyboard::Key::F1;
  } else if (key == Key::F2) {
    return sf::Keyboard::Key::F2;
  } else if (key == Key::F3) {
    return sf::Keyboard::Key::F3;
  } else if (key == Key::F4) {
    return sf::Keyboard::Key::F4;
  } else if (key == Key::F5) {
    return sf::Keyboard::Key::F5;
  } else if (key == Key::F6) {
    return sf::Keyboard::Key::F6;
  } else if (key == Key::F7) {
    return sf::Keyboard::Key::F7;
  } else if (key == Key::F8) {
    return sf::Keyboard::Key::F8;
  } else if (key == Key::F9) {
    return sf::Keyboard::Key::F9;
  } else if (key == Key::F10) {
    return sf::Keyboard::Key::F10;
  } else if (key == Key::F11) {
    return sf::Keyboard::Key::F11;
  } else if (key == Key::F12) {
    return sf::Keyboard::Key::F12;
  } else if (key == Key::F13) {
    return sf::Keyboard::Key::F13;
  } else if (key == Key::F14) {
    return sf::Keyboard::Key::F14;
  } else if (key == Key::F15) {
    return sf::Keyboard::Key::F15;
  } else if (key == Key::Pause) {
    return sf::Keyboard::Key::Pause;
  } else if (key == Key::KeyCount) {
    return sf::Keyboard::Key::KeyCount;
  } else if (key == Key::BackSpace) {
    return sf::Keyboard::Key::BackSpace;
  } else if (key == Key::BackSlash) {
    return sf::Keyboard::Key::BackSlash;
  } else if (key == Key::SemiColon) {
    return sf::Keyboard::Key::SemiColon;
  }

  return sf::Keyboard::Key::Unknown;
}

sf::Mouse::Button InputDataType::getSfmlMouseButton(
    MButton mouse_button) {
  if (mouse_button == MButton::Left) {
    return sf::Mouse::Button::Left;
  } else if (mouse_button == MButton::Right) {
    return sf::Mouse::Button::Right;
  } else if (mouse_button == MButton::Middle) {
    return sf::Mouse::Button::Middle;
  } else if (mouse_button == MButton::XButton1) {
    return sf::Mouse::Button::XButton1;
  } else if (mouse_button == MButton::XButton2) {
    return sf::Mouse::Button::XButton2;
  }

  return sf::Mouse::ButtonCount;
}
}

