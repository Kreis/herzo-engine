#ifndef _INPUT_DATA_TYPE_H_
#define _INPUT_DATA_TYPE_H_

#include <SFML/Graphics.hpp>

namespace Herzo {
enum class Key {
	Unknown = -1,
	A, B, C,
  D, E, F, G,
  H, I, J, K,
  L, M, N, O,
  P, Q, R, S,
  T, U, V, W,
  X, Y, Z, Num0,
  Num1, Num2, Num3, Num4,
  Num5, Num6, Num7, Num8,
  Num9, Escape, LControl, LShift,
  LAlt, LSystem, RControl, RShift,
  RAlt, RSystem, Menu, LBracket,
  RBracket, Comma, Period,
  Quote, Slash, Tilde,
  Equal, Space, Enter,
  Tab, PageUp, PageDown,
  End, Home, Insert, Delete,
  Add, Subtract, Multiply, Divide,
  Left, Right, Up, Down,
  Numpad0, Numpad1, Numpad2, Numpad3,
  Numpad4, Numpad5, Numpad6, Numpad7,
  Numpad8, Numpad9, F1, F2,
  F3, F4, F5, F6,
  F7, F8, F9, F10,
  F11, F12, F13, F14,
  F15, Pause, KeyCount, Dash,
  BackSpace, BackSlash,
	SemiColon
};

enum class MButton {
	Left, Right, Middle, XButton1,
  XButton2, ButtonCount
};

class InputDataType {
public:
  static sf::Keyboard::Key getSfmlKey(Key key);
  static sf::Mouse::Button getSfmlMouseButton(MButton mouse_button);
private:
  InputDataType();
};
}

#endif
