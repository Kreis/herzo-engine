#ifndef _INPUTSTATE_H_
#define _INPUTSTATE_H_

#include <SFML/Graphics.hpp>
#include <Herzo/Common/DataType.h>
#include <Herzo/Input/InputDataType.h>
#include <vector>
#include <array>
#include <functional>
#include <iostream>

namespace Herzo {
class InputState
{
friend class Window;
public:
	~InputState();

	bool isPressed(Key key) const;
	bool isTyped(Key key) const;
	bool isReleased(Key key) const;

	bool isResized() const;

	bool isPressed(MButton mouse_button) const;
	bool isTyped(MButton mouse_button) const;
	bool isReleased(MButton mouse_button) const;

	uint32_t getTextEntered() const;

	iVec getMousePosition();

	static InputState* getSingleton();

	bool closed;

private:
	InputState();
	void updateState(sf::RenderWindow* sfml_render_window);
	static InputState* singleton;
	sf::Event ev;
	std::vector<uint8_t> vecKeyReleased;
	std::vector<uint8_t> vecMouseReleased;
	iVec mousePosition;
	bool resized;
	uint32_t textEntered;
	std::array<uint8_t, sf::Keyboard::Key::KeyCount> bufferKeys;
	std::array<uint8_t, sf::Mouse::Button::ButtonCount> bufferMouseButtons;
};
}

#endif
