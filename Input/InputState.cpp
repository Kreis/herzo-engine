#include "InputState.h"

namespace Herzo {
InputState* InputState::singleton = nullptr;
InputState::InputState() {
	std::fill(
		this->bufferKeys.begin(),
		this->bufferKeys.end(),
		0
	);
	std::fill(
		this->bufferMouseButtons.begin(),
		this->bufferMouseButtons.end(),
		0
	);
	this->closed = false;
	this->resized = false;
	this->textEntered = '\0';
}
InputState::~InputState() {
	if (this->singleton != nullptr) {
		this->singleton = nullptr;
	}
}

void InputState::updateState(sf::RenderWindow* sfml_render_window) {

	// update status typed to pressed
	for (uint8_t i = 0; i < sf::Keyboard::KeyCount; i++) {
		this->bufferKeys[ i ] |=
			(this->bufferKeys[ i ] & 1) << 1;
	}
	for (uint8_t i = 0; i < sf::Mouse::ButtonCount; i++) {
		this->bufferMouseButtons[ i ] |= 
			(this->bufferMouseButtons[ i ] & 1) << 1;
	}

	this->vecKeyReleased.clear();
	this->vecMouseReleased.clear();
	this->resized = false;
	this->textEntered = 0;

	while (sfml_render_window->pollEvent(this->ev)) switch (this->ev.type) {
	case sf::Event::Closed:
		this->closed = true;
		break;

	case sf::Event::KeyPressed:
		this->bufferKeys[ this->ev.key.code ] |= 1;
		break;

	case sf::Event::KeyReleased:
		this->bufferKeys[ this->ev.key.code ] = 0;
		this->vecKeyReleased.push_back(this->ev.key.code);
		break;
	
	case sf::Event::MouseButtonPressed:
		this->bufferMouseButtons[ this->ev.mouseButton.button ] |= 1;
		break;
	
	case sf::Event::MouseButtonReleased:
		this->bufferMouseButtons[ this->ev.mouseButton.button ] = 0;
		this->vecMouseReleased.push_back(this->ev.mouseButton.button);
		break;
	
	case sf::Event::TextEntered:
		this->textEntered = this->ev.text.unicode;
		break;

  	case sf::Event::LostFocus:
		std::fill(
			this->bufferKeys.begin(),
			this->bufferKeys.end(),
			0
		);
		std::fill(
			this->bufferMouseButtons.begin(),
			this->bufferMouseButtons.end(),
			0
		);
		break;
	
	case sf::Event::Resized:
		this->resized = true;
	}

	sf::Vector2i sfmlMousePosition =
    sf::Mouse::getPosition(*sfml_render_window);
	this->mousePosition.x = sfmlMousePosition.x;
	this->mousePosition.y = sfmlMousePosition.y;
}

bool InputState::isPressed(Key key) const {
  sf::Keyboard::Key sfml_key =
    InputDataType::getSfmlKey(key);
	return this->bufferKeys[ sfml_key ] > 0;
}

bool InputState::isTyped(Key key) const {
  sf::Keyboard::Key sfml_key =
    InputDataType::getSfmlKey(key);
	return this->bufferKeys[ sfml_key ] == 1;
}

bool InputState::isReleased(Key key) const {
  sf::Keyboard::Key sfml_key =
    InputDataType::getSfmlKey(key);
	for (const uint8_t& i : this->vecKeyReleased) {
		if (i == sfml_key)
			return true;
	}

	return false;
}

bool InputState::isPressed(MButton mouse_button) const {
  sf::Mouse::Button button = InputDataType::getSfmlMouseButton(mouse_button);
	return this->bufferMouseButtons[ button ] > 0;
}

bool InputState::isTyped(MButton mouse_button) const {
  sf::Mouse::Button button = InputDataType::getSfmlMouseButton(mouse_button);
	return this->bufferMouseButtons[ button ] == 1;
}

bool InputState::isReleased(MButton mouse_button) const {
  sf::Mouse::Button button = InputDataType::getSfmlMouseButton(mouse_button);
	for (const uint8_t& i : this->vecMouseReleased) {
		if (i == button)
			return true;
	}

	return false;
}

uint32_t InputState::getTextEntered() const {
	return this->textEntered;
}

bool InputState::isResized() const {
	return this->resized;
}

iVec InputState::getMousePosition() {
	return this->mousePosition;
}

InputState* InputState::getSingleton() {
	if (singleton == nullptr) {
		singleton = new InputState();
	}

	return singleton;
}
}

