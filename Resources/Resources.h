#ifndef _RESOURCES_H_
#define _RESOURCES_H_

#include <SFML/Graphics.hpp>
#include <vector>
#include <assert.h>

namespace Herzo {
class Resources {
public:
  ~Resources();
  static Resources* getSingleton();
  sf::RenderStates* getRenderStates(std::string path);

private:
  std::vector<std::string> name_render_states;
  std::vector<sf::RenderStates> render_states;
  std::vector<sf::Texture> textures;
  uint32_t vectors_limit;
  static Resources* singleton;
  Resources();
};
}

#endif
