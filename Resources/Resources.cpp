#include "Resources.h"

namespace Herzo {
Resources* Resources::singleton = nullptr;

Resources::~Resources() {
  if (singleton != nullptr) {
    delete singleton;
  }
}

Resources* Resources::getSingleton() {
  if (singleton == nullptr) {
    singleton = new Resources();
  }

  return singleton;
}

sf::RenderStates* Resources::getRenderStates(std::string path) {
  for (uint32_t i = 0; i < vectors_limit; i++) {
    if (name_render_states[ i ] == path) {
      return &render_states[ i ];
    }
  }
  uint32_t id = vectors_limit;
  vectors_limit++;
  assert(vectors_limit < 200);

  name_render_states[ id ] = path;
  textures[ id ].loadFromFile(path);
  render_states[ id ].texture = &textures[ id ];
  return &render_states[ id ];
}

// private
Resources::Resources() {
  name_render_states = std::vector<std::string>(200);
  render_states = std::vector<sf::RenderStates>(200);
  textures = std::vector<sf::Texture>(200);
  vectors_limit = 0;
}
}

