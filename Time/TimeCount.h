#ifndef _TIME_COUNT_H_
#define _TIME_COUNT_H_

#include <SFML/Graphics.hpp>
#include <iostream>
#include <functional>

namespace Herzo {
class TimeCount {
public:
  TimeCount();
  ~TimeCount();
  float elapsedTime();
  float fixedElapsedTime();
  bool showFPS = false;

private:
  const sf::Time timePerFrame = sf::seconds(1.f / 60.f);
  const sf::Time maxTimePerFrame = sf::seconds(20.f / 60.f);
  const float delta = timePerFrame.asSeconds();
  sf::Time timeSinceLastUpdate = sf::Time::Zero;
  sf::Time fps = sf::Time::Zero;
  uint32_t fpsCont = 0;
  sf::Time oneSecond = sf::seconds(1.f);
  sf::Time currentElapsedTime = sf::Time::Zero;

  sf::Clock clock;
  sf::Time time;
};
}

#endif
