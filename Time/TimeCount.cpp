#include "TimeCount.h"

namespace Herzo {
TimeCount::TimeCount() {}
TimeCount::~TimeCount() {}

float TimeCount::elapsedTime() {

  this->currentElapsedTime = clock.restart();
  if (this->currentElapsedTime > maxTimePerFrame) {
    this->currentElapsedTime = maxTimePerFrame;
  }

  if (this->showFPS) {
    this->fps += this->currentElapsedTime;
    if (this->fps > this->oneSecond) {
      std::cout << "Fps: " << this->fpsCont << std::endl;
      this->fps = sf::Time::Zero;
      this->fpsCont = 0;
    }
    this->fpsCont++;
  }

  return this->currentElapsedTime.asSeconds();
}

float TimeCount::fixedElapsedTime() {

  this->timeSinceLastUpdate += this->currentElapsedTime;
  if (this->timeSinceLastUpdate > this->maxTimePerFrame) {
    this->timeSinceLastUpdate = this->maxTimePerFrame;
  }

  float nTimes = 0;
  while (this->timeSinceLastUpdate > this->timePerFrame) {
    this->timeSinceLastUpdate -= this->timePerFrame;
    nTimes++;
  }

  if (nTimes >= 1.f) {
    return this->delta * nTimes;
  }

  return 0.f;
}
}

