#include "Sprite.h"

namespace Herzo {
Sprite::Sprite() {}
Sprite::~Sprite() {}

std::unique_ptr<Sprite> Sprite::create(
    const std::string& texture_path,
    const iRect& tex_rect,
    const iRect& bounds,
    uint32_t color
) {
    std::unique_ptr<Sprite> sprite =
        std::unique_ptr<Sprite>(new Sprite());
    
    sprite->texture_path = texture_path;
    
    sprite->tex[ 0 ].x = tex_rect.x;
    sprite->tex[ 0 ].y = tex_rect.y;

    sprite->tex[ 1 ].x = tex_rect.x + tex_rect.w;
    sprite->tex[ 1 ].y = tex_rect.y;

    sprite->tex[ 2 ].x = tex_rect.x + tex_rect.w;
    sprite->tex[ 2 ].y = tex_rect.y + tex_rect.h;

    sprite->tex[ 3 ].x = tex_rect.x;
    sprite->tex[ 3 ].y = tex_rect.y + tex_rect.h;

    sprite->setBounds(bounds);
    sprite->setColor(color);

    return std::move(sprite);
}

std::unique_ptr<Sprite> Sprite::create(
    const std::string& texture_path,
    const iRect& tex_rect,
    const iVec& position,
    uint32_t color) {
  return std::move(Sprite::create(
        texture_path,
        tex_rect,
        { position.x, position.y, tex_rect.w, tex_rect.h },
        color));
}

Sprite* Sprite::setBounds(const iRect& bounds) {

  this->pos[ 0 ].x = bounds.x;
  this->pos[ 0 ].y = bounds.y;

  this->pos[ 1 ].x = bounds.x + bounds.w;
  this->pos[ 1 ].y = bounds.y;

  this->pos[ 2 ].x = bounds.x + bounds.w;
  this->pos[ 2 ].y = bounds.y + bounds.h;

  this->pos[ 3 ].x = bounds.x;
  this->pos[ 3 ].y = bounds.y + bounds.h;

  return this;
}

Sprite* Sprite::setPosition(const fVec& position) {
  fVec size = this->getSize();

  this->pos[ 0 ].x = position.x;
  this->pos[ 0 ].y = position.y;

  this->pos[ 1 ].x = position.x + size.x;
  this->pos[ 1 ].y = position.y;

  this->pos[ 2 ].x = position.x + size.x;
  this->pos[ 2 ].y = position.y + size.y;

  this->pos[ 3 ].x = position.x;
  this->pos[ 3 ].y = position.y + size.y;
  
  return this;
}

Sprite* Sprite::setColor(uint32_t color) {
  this->color = color;

  return this;
}

Sprite* Sprite::flipHorizontal(bool v) {
  if (this->fliped_horizontal != v) {
    this->fliped_horizontal = v;
    std::swap(this->tex[ 0 ], this->tex[ 1 ]);
    std::swap(this->tex[ 2 ], this->tex[ 3 ]);
  }

  return this;
}

fVec Sprite::getPosition() const {
  return { this->pos[ 0 ].x, this->pos[ 0 ].y };
}

fVec Sprite::getSize() const {
  return {
    abs(this->pos[ 2 ].x - this->pos[ 0 ].x),
    abs(this->pos[ 2 ].y - this->pos[ 0 ].y)
  };
}
}
