#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <string>
#include <Herzo/Common/DataType.h>
#include <memory>
#define COLOR_WHITE 4294967295

namespace Herzo {
class Sprite {
friend class SpriteBatch;
public:
  ~Sprite();
  static std::unique_ptr<Sprite> create(
    const std::string& texture_path,
    const iRect& tex_rect,
    const iRect& bounds,
    uint32_t color = COLOR_WHITE
  );
  
  static std::unique_ptr<Sprite> create(
    const std::string& texture_path,
    const iRect& tex_rect,
    const iVec& position,
    uint32_t color = COLOR_WHITE
  );

  Sprite* setPosition(const fVec& position);
  Sprite* setBounds(const iRect& bounds);
  Sprite* setColor(uint32_t color);
  Sprite* flipHorizontal(bool v);
  fVec getPosition() const;
  fVec getSize() const;

private:
  Sprite();

  std::string texture_path;
  fVec tex[ 4 ];
  fVec pos[ 4 ];
  uint32_t color = COLOR_WHITE;
  bool fliped_horizontal = false;
};
}
  
#endif
