#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include <Herzo/Common/DataType.h>
#include <Herzo/Render/RenderTarget.h>
#include <Herzo/Input/InputState.h>

namespace Herzo {
class SpriteBatch;
class Window : public RenderTarget {
public:
  ~Window();

  static std::unique_ptr<Window> create(
      uint32_t width,
      uint32_t height,
      std::string title);

  void clear(uint32_t color);
  
  sf::RenderTarget* getSfmlRenderTarget() const override;
  
  void moveViewTo(float x, float y);
  void moveViewXTo(float x);
  void moveViewYTo(float y);

  fVec getViewPosition() const; // origin coord at center of the screen
  fVec getScreenSize() const;

  void updateEvents();

  void close();

  std::unique_ptr<sf::RenderWindow> render_window = nullptr;

  Window();
private:
  fVec view_position;
  fVec screen_size;
  sf::View view;
  float original_ratio;

  void updateView();
  float getRatio(fVec size) const;
  void updateRatio();
};
}

#endif
