#ifndef _TEXTURE_PRIORITY_H_
#define _TEXTURE_PRIORITY_H_

#include <iostream>
#include <Herzo/Common/DataType.h>

namespace Herzo {
enum class TPType {
  TEXTURE,
  RENDER_TEXTURE
};

class TexturePriority {
friend class SpriteBatch;
public:
  TexturePriority(const std::string& texture_path);
  TexturePriority(
      const std::string& texture_path,
      const iVec& size);

  void setPosition(const iVec& position);
  TPType getType();

  const std::string getTexturePath() const;
private:
  std::string texture_path;
  iVec position;
  iVec size;
};
}

#endif
