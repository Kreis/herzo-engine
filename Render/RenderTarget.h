#ifndef _RENDER_TARGET_H_
#define _RENDER_TARGET_H_

#include <SFML/Graphics.hpp>

class RenderTarget {
public:
  virtual sf::RenderTarget* getSfmlRenderTarget() const = 0;
};


#endif
