#include "TexturePriority.h"

namespace Herzo {
TexturePriority::TexturePriority(const std::string& texture_path) {
  this->texture_path = texture_path;
}

TexturePriority::TexturePriority(
    const std::string& texture_path,
    const iVec& size) {
  this->texture_path = texture_path;
  this->size = size;
}

void TexturePriority::setPosition(const iVec& position) {
  this->position = position;
}

TPType TexturePriority::getType() {
  if (this->size.x > 0 && this->size.y > 0) {
    return TPType::RENDER_TEXTURE;
  }
  return TPType::TEXTURE;
}

const std::string TexturePriority::getTexturePath() const {
  return this->texture_path;
}
           
}
