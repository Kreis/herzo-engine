#include "RenderTexture.h"

#include <Herzo/Render/SpriteBatch.h>

namespace Herzo {
RenderTexture::RenderTexture() {
}

std::unique_ptr<RenderTexture> RenderTexture::create(
      iVec size) {
  std::unique_ptr<RenderTexture> render_texture =
    std::unique_ptr<RenderTexture>(new RenderTexture());

  render_texture->sfml_render_texture =
    std::make_unique<sf::RenderTexture>();
  render_texture->sfml_render_texture->create(
      size.x, size.y);

  render_texture->vertex.resize(4);

  render_texture->vertex[0].texCoords.x = 0;
  render_texture->vertex[0].texCoords.y = 0;

  render_texture->vertex[1].texCoords.x = size.x;
  render_texture->vertex[1].texCoords.y = 0;

  render_texture->vertex[2].texCoords.x = size.x;
  render_texture->vertex[2].texCoords.y = size.y;

  render_texture->vertex[3].texCoords.x = 0;
  render_texture->vertex[3].texCoords.y = size.y;
  
  render_texture->setPosition({ 0, 0 });

  return std::move(render_texture);
}

void RenderTexture::setPosition(fVec position) {
  fVec size = this->getSize();
  this->vertex[ 0 ].position.x = position.x;
  this->vertex[ 0 ].position.y = position.y;

  this->vertex[ 1 ].position.x = position.x + size.x;
  this->vertex[ 1 ].position.y = position.y;

  this->vertex[ 2 ].position.x = position.x + size.x;
  this->vertex[ 2 ].position.y = position.y + size.y;

  this->vertex[ 3 ].position.x = position.x;
  this->vertex[ 3 ].position.y = position.y + size.y;
}

fVec RenderTexture::getSize() {
  const sf::Vertex& two = this->vertex[ 2 ];
  const sf::Vertex& zero = this->vertex[ 0 ];
  return {
    abs(two.texCoords.x - zero.texCoords.x),
    abs(two.texCoords.y - zero.texCoords.y)
  };
}

sf::RenderTarget* RenderTexture::getSfmlRenderTarget() const {
  return this->sfml_render_texture.get();
}

const sf::RenderStates* RenderTexture::getRenderStates() {
  this->sfml_render_states.texture =
    &this->sfml_render_texture->getTexture();

  return &this->sfml_render_states;
}


void RenderTexture::flush(SpriteBatch* sprite_batch) {
  sprite_batch->flush(this);
}
}

