#include "SpriteBatch.h"

namespace Herzo {
SpriteBatch::SpriteBatch() {}
SpriteBatch::~SpriteBatch() {}

std::unique_ptr<SpriteBatch> SpriteBatch::create(
  std::vector<TexturePriority> texture_priority,
  uint32_t initial_buffer) {

  std::unique_ptr<SpriteBatch> sprite_batch =
    std::unique_ptr<SpriteBatch>(new SpriteBatch());
  
  sprite_batch->vec_vertex.clear();
  sprite_batch->vec_last_vertex.clear();
  sprite_batch->initial_buffer = initial_buffer;

  // Texture priority: first in vector, first to draw
  // then, the first in vector is the deepest layer
  sprite_batch->texture_priority = texture_priority;
  std::vector<sf::Vertex> default_vertex(initial_buffer);
  for (TexturePriority& tp : texture_priority) {
    if (tp.getType() == TPType::TEXTURE) {
      sprite_batch->vec_vertex.push_back(default_vertex);
      sprite_batch->vec_last_vertex.push_back(0);
      sprite_batch->vec_batch_updated.push_back(false);

      sf::VertexBuffer default_vertex_buffer(sf::Quads);
      default_vertex_buffer.create(initial_buffer);
      sprite_batch->vec_vertex_buffer.push_back(default_vertex_buffer);
      continue;
    }

    if (tp.getType() == TPType::RENDER_TEXTURE) {
      std::unique_ptr<RenderTexture> render_texture =
        RenderTexture::create(tp.size);
      sprite_batch->vec_render_texture.push_back(
          std::move(render_texture));
      continue;
    }
  }

  return std::move(sprite_batch);
}

void SpriteBatch::clear(
  uint32_t color) {

  uint32_t texture_id = 0;
  uint32_t render_texture_id = 0;
  for (TexturePriority& tp : texture_priority) {
    if (tp.getType() == TPType::TEXTURE) {
      this->vec_last_vertex[ texture_id ] = 0;
      
      // refresh in order to update render when flush
      this->vec_batch_updated[ texture_id ] = true;

      texture_id++;
      continue;
    }

    if (tp.getType() == TPType::RENDER_TEXTURE) {
      this->vec_render_texture[ render_texture_id ]
        ->getSfmlRenderTarget()->clear(sf::Color(color));
      render_texture_id++;
      continue;
    }
  }
}

void SpriteBatch::draw(const Sprite* sprite) {
  uint32_t id = this->getIdTexture(sprite->texture_path);
  this->vec_batch_updated[ id ] = true;
  this->putSprite(vec_vertex[ id ], vec_last_vertex[ id ], sprite);
}

void SpriteBatch::flush(RenderTarget* render_target) {

  // Flush each texture
  for (uint32_t i = 0; i < texture_priority.size(); i++) {
    TexturePriority& tp = texture_priority[ i ];

    if (tp.getType() == TPType::TEXTURE) {
      this->flushTexture(
          render_target,
          tp.getTexturePath());
    }

    if (tp.getType() == TPType::RENDER_TEXTURE) {
      this->flushRenderTexture(
          render_target,
          tp.getTexturePath());
    }
  }

  // Render in window or render texture
  if (sf::RenderWindow* render_window = 
      dynamic_cast<sf::RenderWindow*>(
        render_target->getSfmlRenderTarget())) {
    render_window->display();
  } else if (sf::RenderTexture* render_texture =
      dynamic_cast<sf::RenderTexture*>(
        render_target->getSfmlRenderTarget())) {
    render_texture->display();
  }
}


void SpriteBatch::flushTexture(
    RenderTarget* render_target,
    const std::string& texture_path) {

  uint32_t texture_id = this->getIdTexture(texture_path);

  // if this texture was not updated, nothing to do here
  if ( ! this->vec_batch_updated[ texture_id ]) {
    return;
  }

  std::vector<sf::Vertex>& current_vector =
    this->vec_vertex[ texture_id ];

  sf::RenderStates* render_states = 
    Resources::getSingleton()->
      getRenderStates(texture_path);

  if (this->vec_last_vertex[ texture_id ] > 0) {
    this->vec_vertex_buffer[ texture_id ].update(
        &current_vector[ 0 ],
        vec_last_vertex[ texture_id ],
        /*offset=*/0);
    render_target->getSfmlRenderTarget()->draw(
        this->vec_vertex_buffer[ texture_id ],
        *render_states);
  }
}

void SpriteBatch::flushRenderTexture(
    RenderTarget* render_target,
    const std::string& texture_path) {

  RenderTexture* render_texture = this->getRenderTexture(
      texture_path);

  if ( ! render_texture->enabled) {
    return;
  }
  
  render_target->getSfmlRenderTarget()->draw(
      &render_texture->vertex[ 0 ],
      4,
      sf::Quads,
      *render_texture->getRenderStates());
}

std::vector<TexturePriority> SpriteBatch::getTexturePriority() {
  	return this->texture_priority;
}

RenderTexture* SpriteBatch::getRenderTexture(
    const std::string& texture_path) {
  uint32_t i = 0;
  for (TexturePriority& tp : this->texture_priority) {
    if (tp.getType() != TPType::RENDER_TEXTURE) {
      continue;
    }
    if (tp.getTexturePath() != texture_path) {
      i++;
      continue;
    }

    return this->vec_render_texture[ i ].get();
  }

  LOG("ERROR: search for a render without priority");
  LOG(texture_path);
  PANIC;
  return nullptr;
}

// private
uint32_t SpriteBatch::getIdTexture(const std::string& texture_path) {
  uint32_t i = 0;
  for (TexturePriority& tp : this->texture_priority) {
    if (tp.getType() != TPType::TEXTURE) {
      continue;
    }
    if (tp.getTexturePath() != texture_path) {
      i++;
      continue;
    }

    return i;
  }

  LOG("search for a texture without priority");
  LOG(texture_path);
  PANIC;
  return 0;
}

void SpriteBatch::putSprite(
    std::vector<sf::Vertex>& vertex,
    uint32_t& last_vertex,
    const Sprite* sprite) {

  if (last_vertex >= vertex.size()) {
    vertex.push_back(sf::Vertex());
    vertex.push_back(sf::Vertex());
    vertex.push_back(sf::Vertex());
    vertex.push_back(sf::Vertex());
  }

  // Screen coordinate
  vertex[ last_vertex + 0 ].position.x = sprite->pos[ 0 ].x;
  vertex[ last_vertex + 0 ].position.y = sprite->pos[ 0 ].y;

  vertex[ last_vertex + 1 ].position.x = sprite->pos[ 1 ].x;
  vertex[ last_vertex + 1 ].position.y = sprite->pos[ 1 ].y;

  vertex[ last_vertex + 2 ].position.x = sprite->pos[ 2 ].x;
  vertex[ last_vertex + 2 ].position.y = sprite->pos[ 2 ].y;

  vertex[ last_vertex + 3 ].position.x = sprite->pos[ 3 ].x;
  vertex[ last_vertex + 3 ].position.y = sprite->pos[ 3 ].y;

  // Texture coordinate
  vertex[ last_vertex + 0 ].texCoords.x = sprite->tex[ 0 ].x;
  vertex[ last_vertex + 0 ].texCoords.y = sprite->tex[ 0 ].y;

  vertex[ last_vertex + 1 ].texCoords.x = sprite->tex[ 1 ].x;
  vertex[ last_vertex + 1 ].texCoords.y = sprite->tex[ 1 ].y;

  vertex[ last_vertex + 2 ].texCoords.x = sprite->tex[ 2 ].x;
  vertex[ last_vertex + 2 ].texCoords.y = sprite->tex[ 2 ].y;

  vertex[ last_vertex + 3 ].texCoords.x = sprite->tex[ 3 ].x;
  vertex[ last_vertex + 3 ].texCoords.y = sprite->tex[ 3 ].y;

  // Color
  vertex[ last_vertex + 0 ].color = sf::Color(sprite->color);
  vertex[ last_vertex + 1 ].color = sf::Color(sprite->color);
  vertex[ last_vertex + 2 ].color = sf::Color(sprite->color);
  vertex[ last_vertex + 3 ].color = sf::Color(sprite->color);

  last_vertex += 4;
}
}
