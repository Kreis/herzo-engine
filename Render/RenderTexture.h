#ifndef _RENDER_TEXTURE_H_
#define _RENDER_TEXTURE_H_

#include <SFML/Graphics.hpp>
#include <memory>
#include <Herzo/Render/RenderTarget.h>
#include <Herzo/Common/DataType.h>

namespace Herzo {
class SpriteBatch;
class RenderTexture : public RenderTarget {
public:
  static std::unique_ptr<RenderTexture> create(iVec size);
  void setPosition(fVec position);
  fVec getSize();
  sf::RenderTarget* getSfmlRenderTarget() const override;
  const sf::RenderStates* getRenderStates();
  bool enabled = true;
  std::vector<sf::Vertex> vertex;
  void flush(SpriteBatch* sprite_batch);
  // TODO put a flush with sprite batch

private:
  RenderTexture();
  std::unique_ptr<sf::RenderTexture> sfml_render_texture = nullptr;
  sf::RenderStates sfml_render_states;
};
}

#endif
