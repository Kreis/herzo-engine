#include "Window.h"

#include "Herzo/Render/SpriteBatch.h"

namespace Herzo {
Window::Window() {}
Window::~Window() {}

std::unique_ptr<Window> Window::create(uint32_t width, uint32_t height, std::string title) {
  std::unique_ptr<Window> window = std::unique_ptr<Window>(new Window());
  window->screen_size.x = width;
  window->screen_size.y = height;
  window->original_ratio =
    static_cast<float>(width) / static_cast<float>(height);
  window->render_window =
    std::make_unique<sf::RenderWindow>(
        sf::VideoMode(width, height), title);
  window->render_window->setVerticalSyncEnabled(true);

  window->view_position.x = width / 2;
  window->view_position.y = height / 2;

  return std::move(window);
}

void Window::clear(uint32_t color) {
  this->render_window->clear(sf::Color(color));
}

sf::RenderTarget* Window::getSfmlRenderTarget() const {
  return this->render_window.get();
}

fVec Window::getViewPosition() const {
  return this->view_position;
}

fVec Window::getScreenSize() const {
  sf::Vector2u size = render_window->getSize();
  return {
    static_cast<int32_t>(size.x),
    static_cast<int32_t>(size.y)
  };
}

void Window::updateEvents() {
  InputState::getSingleton()->updateState(
    this->render_window.get());

  if (InputState::getSingleton()->resized) {
    this->updateRatio();
  }

}

void Window::moveViewTo(float x, float y) {
  this->view_position.x = x;
  this->view_position.y = y;
  this->updateView();
}

void Window::moveViewXTo(float x) {
  this->view_position.x = x;
  this->updateView();
}

void Window::moveViewYTo(float y) {
  this->view_position.y = y;
  this->updateView();
}

void Window::updateView() {
  sf::View view = this->render_window->getDefaultView();
  view.setCenter(
    sf::Vector2f(
      this->view_position.x, this->view_position.y
    ));
  this->render_window->setView(view);
}

void Window::close() {
  this->render_window->close();
}

float Window::getRatio(fVec size) const {
  return size.x / size.y;
}


void Window::updateRatio() {
  fVec new_screen_size = this->getScreenSize();
  float new_ratio = this->getRatio(new_screen_size);

  // touch first the width
  if (new_ratio < this->original_ratio) {
    float new_height = new_screen_size.x / this->original_ratio;
    float offset =
      (new_screen_size.y - new_height) / 2;
    this->view = this->render_window->getDefaultView();
    this->view.setViewport(
      sf::FloatRect(
        0.f,
        offset / new_screen_size.y,
        1.f,
        (new_screen_size.y - (2 * offset)) / 
          new_screen_size.y
      ));

    this->render_window->setView(this->view);
  }

  // touch first the height
  if (new_ratio > this->original_ratio) {
    float new_width = new_screen_size.y * this->original_ratio;
    float offset =
      (new_screen_size.x - new_width) / 2;
    this->view = this->render_window->getDefaultView();
    this->view.setViewport(
      sf::FloatRect(
        offset / new_screen_size.x,
        0.f,
        (new_screen_size.x - (2 * offset)) / 
          new_screen_size.x,
        1.f
      ));
  }

  this->render_window->setView(this->view);
}
}

