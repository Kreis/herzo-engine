#ifndef _SPRITE_BATCH_H_
#define _SPRITE_BATCH_H_

#include <SFML/Graphics.hpp>
#include <Herzo/Render/Sprite.h>
#include <Herzo/Render/TexturePriority.h>
#include <Herzo/Render/RenderTarget.h>
#include <Herzo/Render/RenderTexture.h>
#include <Herzo/Resources/Resources.h>
#include <Herzo/Common/Log.h>
#include <memory>
#include <assert.h>

namespace Herzo {
class SpriteBatch {
friend class RenderTexture;
public:
  ~SpriteBatch();

  static std::unique_ptr<SpriteBatch> create(
    std::vector<TexturePriority> texture_priority,
    uint32_t initial_buffer = 10000);

  void clear(
    uint32_t color = COLOR_WHITE);

  void draw(const Sprite* sprite);

  void flush(RenderTarget* render_target);

  std::vector<TexturePriority> getTexturePriority();
  RenderTexture* getRenderTexture(
      const std::string& texture_path);

private:

  // this specify if a single vertex buffer should update
  // (if we have any changed)
  std::vector<bool> vec_batch_updated;

  SpriteBatch();
  std::vector<TexturePriority> texture_priority;

  void flushTexture(
      RenderTarget* render_target,
      const std::string& texture_path);
  void flushRenderTexture(
      RenderTarget* render_target,
      const std::string& texture_path);
  
  // For texture
  std::vector<std::vector<sf::Vertex> > vec_vertex;
  std::vector<sf::VertexBuffer> vec_vertex_buffer;
  // Each uint32_t for texture
  std::vector<uint32_t> vec_last_vertex;

  // for render texture
  std::vector<std::unique_ptr<RenderTexture> > vec_render_texture;
  uint32_t initial_buffer;

  uint32_t getIdTexture(const std::string& texture_path);
  void putSprite(
      std::vector<sf::Vertex>& vertex,
      uint32_t& last_vertex,
      const Sprite* sprite);
};
}

#endif
