# Herzo

## OSX compiling

```sh
#requires sfml lib
g++ -std=c++17 -c */*.cpp -I.. -I../SFML/osx/include -L../SFML/osx/lib -Wno-narrowing && ar rvs herzo.a *.o && rm *.o
```

## Unix compiling

```sh
# required sfml lib
g++ -std=c++17 -c */*.cpp -I.. -I../SFML/unix/include -L../SFML/unix/lib -Wno-narrowing && ar rvs herzo.a *.o && rm *.o
```

## Windows compiling

```sh
# all .cpp files recursively
# (Get-ChildItem -Path . -Name -Recurse -Include *.cpp)

g++ -std=c++17 -c (Get-ChildItem -Path . -Name -Recurse -Include *.cpp) -I.. -IC:\SFML\include -LC:\SFML\lib -lsfml-graphics -lsfml-window -lsfml-system -lopengl32 -Wno-narrowing
# in case of failure linking .a file, add `-g` at the end of compiling Herzo

ar rvs herzo.a *.o

rm *.o
```
