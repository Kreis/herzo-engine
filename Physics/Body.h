#ifndef _BODY_H_
#define _BODY_H_

#include <Herzo/Common/DataType.h>

namespace Herzo {
class Slope;
class Body {
friend class Physics;
public:
	~Body();

	vf getX() const;
	vf getY() const;
	vf getWidth() const;
	vf getHeight() const;

	void setPosition(float x, float y);
	void setType(int32_t v);

	void move(vf x, vf y);
	void moveTo(vf x, vf y);
	bool isColliding() const;
	void off();

private:
	Body();
	void init(vf x, vf y, vf width, vf height, bool isDynamic);
	vf x;
	vf y;
	vf width;
	vf height;
	hVec tryingMove;
	
	bool slopePriority;
	bool isDynamic;
	bool alive; // inactive
	bool buried; // ready to be taken as a new hbody
	bool colliding;

	int32_t type = 0;
};
}

#endif
