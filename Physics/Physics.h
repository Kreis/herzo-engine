#ifndef _PHYSICS_H_
#define _PHYSICS_H_

#include <Herzo/Common/DataType.h>
#include <Herzo/Physics/Body.h>
#include <Herzo/Physics/Slope.h>
#include <vector>
#include <stack>

namespace Herzo {
class Body;
class Slope;
class Physics {
public:
	~Physics();

	void update();
  uint32_t generateBody(vf x, vf y, vf width, vf height, bool isDynamic = false);
  uint32_t generateSlope(hVec up, hVec down);

  Body* getBody(const uint32_t id);
  Slope* getSlope(const uint32_t id);

	static Physics* getSingleton();
private:
	Physics();
	static Physics* singleton;
	std::stack<uint32_t> availableBody;
  std::stack<uint32_t> availableSlope;
	std::vector<Body> bodyPool;
	std::vector<Slope> slopePool;

  vf nx, ny, nw, nh;
  hVec upLeft, upRight, downLeft, downRight;
  vf area1, area2;

	void checkCollideForSlope(Body& A, Slope& S);
	void checkCollideForBodyInX(Body& A, Body& B);
  void checkCollideForBodyInY(Body& A, Body& B);
  bool tryingIsCollidingLeft(Body& A, Body& B);
  bool tryingIsCollidingRight(Body& A, Body& B);
  bool tryingIsCollidingUp(Body& A, Body& B);
  bool tryingIsCollidingDown(Body& A, Body& B);
  bool isInContact(Body& A, Slope& S);
  bool linesColliding(hVec a1, hVec a2, hVec b1, hVec b2);
  uint8_t clockWise(hVec a, hVec b, hVec c);
  vf getYOnSlopeByX(vf x, Slope& S);
};
}

#endif
