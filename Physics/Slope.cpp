#include "Slope.h"

namespace Herzo {
Slope::Slope(hVec up, hVec down) :
  up(up), down(down),
  alive(true), buried(false) {
  calculateType();
  m = (up.y - down.y) / (up.x - down.x);
}
Slope::~Slope() {}

void Slope::off() {
	alive = false;
}

void Slope::calculateType() {
	if (down.x < up.x) {
		type = SlopeType::ToFaceLeft;
	} else {
		type = SlopeType::ToFaceRight;
	}
}
}
