#ifndef _SLOPE_H_
#define _SLOPE_H_

#include <Herzo/Common/DataType.h>

namespace Herzo {
enum class SlopeType {
	ToFaceLeft,
	ToFaceRight
};

class Slope {
friend class Physics;
public:
	~Slope();

	hVec up;
	hVec down;

	SlopeType type;

	void off();
private:
	Slope(hVec up, hVec down);
	bool alive;
  bool buried;
  vf m;
	void calculateType();
};
}

#endif
