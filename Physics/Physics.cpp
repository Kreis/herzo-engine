#include "Physics.h"
#include <iostream>

#define ANTI_CLOCKWISE 1
#define CLOCKWISE 2
#define CONSECUTIVE 0

namespace Herzo {
Physics* Physics::singleton = nullptr;
Physics::Physics() {}
Physics::~Physics() {
  if (singleton != nullptr) {
    delete singleton;
  }
}

void Physics::update() {
	for (uint32_t i = 0; i < bodyPool.size(); i++) {
    
    // ignore dead bodys
		if ( ! bodyPool[ i ].alive) {
      // put it in the pool
      if ( ! bodyPool[ i ].buried) {
        bodyPool[ i ].buried = true;
        availableBody.push(i);
      }
			continue;
		}

    if ( ! bodyPool[ i ].isDynamic) {
      continue;
    }

    bodyPool[ i ].colliding = false;

    // update vs slope
    bodyPool[ i ].slopePriority = false;
		for (uint32_t j = 0; j < slopePool.size(); j++) if (
      slopePool[ j ].alive) {
      checkCollideForSlope(bodyPool[ i ], slopePool[ j ]);
		}
    
    if ( ! bodyPool[ i ].slopePriority) {
      // update vs another body [ X ]
      for (uint32_t j = 0; j < bodyPool.size(); j++) if (
        i != j &&
        bodyPool[ j ].alive &&
        (bodyPool[ i ].type != bodyPool[ j ].type || bodyPool[ i ].type == 0)
      ) {
        checkCollideForBodyInX(bodyPool[ i ], bodyPool[ j ]);
      }
    }
    // update free move
    bodyPool[ i ].x += bodyPool[ i ].tryingMove.x;
    bodyPool[ i ].tryingMove.x = 0;

    if ( ! bodyPool[ i ].slopePriority) {
      // update vs another body [ Y ]
      for (uint32_t j = 0; j < bodyPool.size(); j++) if (
        i != j &&
        bodyPool[ j ].alive &&
        (bodyPool[ i ].type != bodyPool[ j ].type || bodyPool[ i ].type == 0)
      ) {
        checkCollideForBodyInY(bodyPool[ i ], bodyPool[ j ]);
      }
    }
    // update free move
		bodyPool[ i ].y += bodyPool[ i ].tryingMove.y;
		bodyPool[ i ].tryingMove.y = 0;
	}

  // verify dead slopes
	for (uint32_t i = 0; i < slopePool.size(); i++) {
    if ( ! slopePool[ i ].alive && ! slopePool[ i ].buried) {
      availableSlope.push(i);
    }
	}
}

uint32_t Physics::generateBody(vf x, vf y, vf width, vf height, bool isDynamic) {
	if (availableBody.empty()) {
    bodyPool.push_back(Body());
    bodyPool[bodyPool.size() - 1].init(x, y, width, height, isDynamic);
    return static_cast<uint32_t>(bodyPool.size() - 1);
	}
	
  uint32_t availableId = availableBody.top();
  availableBody.pop();

	bodyPool[ availableId ].alive = true;
  bodyPool[ availableId ].buried = false;
  bodyPool[ availableId ].x = x;
  bodyPool[ availableId ].y = y;
  bodyPool[ availableId ].width = width;
  bodyPool[ availableId ].height = height;
  bodyPool[ availableId ].isDynamic = isDynamic;
  bodyPool[ availableId ].colliding = false;

  return availableId;
}

uint32_t Physics::generateSlope(hVec up, hVec down) {
	if (availableSlope.empty()) {
    slopePool.push_back(Slope(up, down));
    return static_cast<uint32_t>(slopePool.size() - 1);
	}

  uint32_t availableId = availableSlope.top();
  availableSlope.pop();

  slopePool[ availableId ].alive = true;
  slopePool[ availableId ].buried = false;
  slopePool[ availableId ].up.x = up.x;
  slopePool[ availableId ].up.y = up.y;
  slopePool[ availableId ].down.x = down.x;
  slopePool[ availableId ].down.y = down.y;
  slopePool[ availableId ].calculateType();
	
  return availableId;
}

Body* Physics::getBody(const uint32_t id) {
  return &bodyPool[ id ];
}

Slope* Physics::getSlope(const uint32_t id) {
  return &slopePool[ id ];
}

Physics* Physics::getSingleton() {
  if (singleton == nullptr) {
    singleton = new Physics();
  }

	return singleton;
}

void Physics::checkCollideForSlope(Body& A, Slope& S) {
	// if A is moving up do nothing
  if (A.tryingMove.y < 0) {
    return;
  }

	// if A is in contact with S, S define the new trying move of A
  if (isInContact(A, S)) {
    A.colliding = true;
    A.slopePriority = true;
    if (S.type == SlopeType::ToFaceLeft) {
      nx = A.x + A.tryingMove.x + A.width;
      if (nx <= S.down.x) {
        A.tryingMove.y = S.down.y - A.y - A.height;
      } else if (nx >= S.up.x) {
        A.tryingMove.y = S.up.y - A.y - A.height;
      } else {
        A.tryingMove.y = getYOnSlopeByX(nx, S) - A.y - A.height;
      }
    } else if (S.type == SlopeType::ToFaceRight) {
      nx = A.x + A.tryingMove.x;
      if (nx >= S.down.x) {
        A.tryingMove.y = S.down.y - A.y - A.height;
      } else if (nx <= S.up.x) {
        A.tryingMove.y = S.up.y - A.y - A.height;
      } else {
        A.tryingMove.y = getYOnSlopeByX(nx, S) - A.y - A.height;
      }
    }
    return;
  }

	// if A wil collide to S stop A on collide point
  if (S.type == SlopeType::ToFaceLeft) {
    
    nx = A.x + A.tryingMove.x + A.width;
    ny = A.y + A.tryingMove.y + A.height;

    // On top
    if (A.x + A.tryingMove.x <= S.up.x && nx >= S.up.x) {
      downLeft = { A.x, ny };
      downRight = { nx, ny };
      if (linesColliding(downLeft, downRight, S.up, S.down)) {
        A.tryingMove.y = S.up.y - A.y - A.height;
        A.slopePriority = true;
        A.colliding = true;
        return;
      }
    }

    // On ramp
    if (nx >= S.down.x && nx <= S.up.x) {
      upRight = { nx, A.y + A.tryingMove.y };
      downRight = { nx, ny };
      if (linesColliding(upRight, downRight, S.up, S.down)) {
        A.tryingMove.y = getYOnSlopeByX(nx, S) - A.y - A.height;
        A.slopePriority = true;
        A.colliding = true;
        return;
      }
    }
  }
  if (S.type == SlopeType::ToFaceRight) {

    nx = A.x + A.tryingMove.x;
    ny = A.y + A.tryingMove.y;

    // On top
    vf s = nx + A.width;
    if (nx <= S.up.x && nx + A.width >= S.up.x) {
      downLeft = { nx, ny + A.height };
      downRight = { nx + A.width, ny + A.height };
      if (linesColliding(downLeft, downRight, S.up, S.down)) {
        A.tryingMove.y = S.up.y - A.y - A.height;
        A.slopePriority = true;
        A.colliding = true;
        return;
      }
    }

    // On ramp
    if (nx + A.width >= S.up.x && nx <= S.down.x) {
      upLeft = { nx, ny };
      downLeft = { nx, ny + A.height };
      if (linesColliding(upLeft, downLeft, S.up, S.down)) {
        A.tryingMove.y = getYOnSlopeByX(nx, S) - A.y - A.height;
        A.slopePriority = true;
        A.colliding = true;
        return;
      }
    }
  }
}

void Physics::checkCollideForBodyInX(Body& A, Body& B) {
  if (A.tryingMove.x < 0 && tryingIsCollidingLeft(A, B)) {
    A.tryingMove.x = B.x + B.width - A.x;
    A.colliding = true;
  } else if (A.tryingMove.x > 0 && tryingIsCollidingRight(A, B)) {
    A.tryingMove.x = B.x - A.x - A.width;
    A.colliding = true;
  }
}

void Physics::checkCollideForBodyInY(Body& A, Body& B) {
  if (A.tryingMove.y > 0 && tryingIsCollidingDown(A, B)) {
    A.tryingMove.y = B.y - A.y - A.height;
    A.colliding = true;
  } else if (A.tryingMove.y < 0 && tryingIsCollidingUp(A, B)) {
    A.tryingMove.y = B.y + B.height - A.y;
    A.colliding = true;
  }
}

bool Physics::tryingIsCollidingLeft(Body& A, Body& B) {
  return A.x + A.tryingMove.x + A.width > B.x && A.x + A.tryingMove.x < B.x + B.width &&
    A.y + A.height > B.y && A.y < B.y + B.height;
}

bool Physics::tryingIsCollidingRight(Body& A, Body& B) {
   return A.x + A.tryingMove.x + A.width > B.x && A.x < B.x + B.width &&
    A.y + A.height > B.y && A.y < B.y + B.height;
}


bool Physics::tryingIsCollidingUp(Body& A, Body& B) {
  return A.x + A.width > B.x && A.x < B.x + B.width &&
    A.y + A.tryingMove.y + A.height > B.y && A.y + A.tryingMove.y < B.y + B.height;
}

bool Physics::tryingIsCollidingDown(Body& A, Body& B) {
  return A.x + A.width > B.x && A.x < B.x + B.width &&
    A.y + A.tryingMove.y + A.height > B.y && A.y < B.y + B.height;
}

bool Physics::isInContact(Body& A, Slope& S) {
  // On top
  if (A.x <= S.up.x && A.x + A.width > S.up.x && A.y + A.height == S.up.y) {
    return true;
  }

  if (S.type == SlopeType::ToFaceLeft) {
    nx = A.x + A.width;
    if (nx >= S.down.x && nx <= S.up.x && A.y + A.height == getYOnSlopeByX(nx, S)) {
        return true;
    }
  }

  if (S.type == SlopeType::ToFaceRight) {
    if (A.x >= S.up.x && A.x <= S.down.x && A.y + A.height == getYOnSlopeByX(A.x, S)) {
      return true;
    }
  }

  return false;
}

bool Physics::linesColliding(hVec a1, hVec a2, hVec b1, hVec b2) {
  if (clockWise(a1, b1, a2) == clockWise(a1, b2, a2))
    return false;

  if (clockWise(b1, a1, b2) == clockWise(b1, a2, b2))
    return false;

  return true;
}

uint8_t Physics::clockWise(hVec a, hVec b, hVec c) {
  area1 = (a.y - b.y) * (c.x - a.x);
  area2 = (b.x - a.x) * (a.y - c.y);
  if (area1 < area2) {
    return ANTI_CLOCKWISE;
  }

  if (area2 > area1) {
    return CLOCKWISE;
  }

  return CONSECUTIVE;
}

vf Physics::getYOnSlopeByX(vf x, Slope& S) {
  return (S.m * x) + S.up.y - (S.m * S.up.x);
}
}
