#ifndef _FONT_CONFIG_H_
#define _FONT_CONFIG_H_

#include <Herzo/Common/DataType.h>
#include <memory>
#include <map>
#include "Letter.h"
#include <vector>
#include <assert.h>

namespace Herzo {
class FontConfig {
public:
    FontConfig();

    bool validate();
    bool isValidated() const;

    // size of the row height
    void setLineHeight(uint32_t line_height);

    // space is handled independently
    // (not as character)
    void setSpaceWidth(uint32_t space_width);

    void setKernings(
        const std::vector<uint8_t>& firsts,
		const std::vector<uint8_t>& seconds,
		const std::vector<int8_t>& amounts
    );
    void setLetters(
        const std::vector<uint8_t>& ascii,
        const std::vector<iRect>& texture_area,
        const std::vector<iVec>& offset_position,
        const std::vector<uint32_t>& xadvance,
        const std::vector<std::string>& textures_name
    );

    uint32_t getLineHeight() const;
    uint32_t getSpaceWidth() const;
    float getLetterDelay() const;
    const Letter* getLetter(uint8_t ascii) const;
    int8_t getKerning(uint8_t a, uint8_t b) const;
	
private:
    std::vector<Letter> letters;
    std::map<uint8_t, std::map<uint8_t, int8_t> > map_kerning;
    std::map<uint8_t, uint32_t> map_letter_index;
    uint32_t line_height = 0;
    uint32_t space_width = 0;
    float letter_delay = 0.0f;
    bool validated = false;
};
}

#endif
