#include "TextBox.h"

namespace Herzo {
TextBox::TextBox() {
    this->sprites = std::vector<std::unique_ptr<Sprite> >(1000);
}

std::unique_ptr<TextBox> TextBox::create(
    const FontConfig& font_config,
    const iRect& box) {

  std::unique_ptr<TextBox> text_box =
    std::unique_ptr<TextBox>(new TextBox());
  text_box->box = box;
  text_box->font_config = font_config;

  return std::move(text_box);
}

void TextBox::setBox(const iRect& box) {
  this->box = box;
  this->setText(
      std::wstring(this->tool_text.begin(), this->tool_text.end()),
      this->color
  );
}

void TextBox::setText(std::wstring given_text, int32_t color) {
  assert(this->font_config.isValidated());
  this->color = color;
  iVec cursor = { this->box.x, this->box.y };
  cursor.y -= this->font_config.getLineHeight();

  this->tool_text.clear();
  // Adjust just for the last letters that fit
  uint32_t current_length = 0;
  for (int32_t i = given_text.size() - 1; i >= 0; i--) {
    uint32_t c = given_text[ i ];
    const Letter* letter = this->font_config.getLetter(c);
    if (letter == nullptr) {
      continue;
    }
    current_length += letter->xadvance;

    if (current_length > this->box.w) {
      break;
    }

    this->tool_text.push_back(letter->letter);
  }

  std::reverse(this->tool_text.begin(), this->tool_text.end());
  uint32_t last_c = 0;
  for (uint32_t i = 0; i < this->tool_text.size(); i++) {
    uint32_t c = tool_text[ i ];

    int8_t kerning = this->font_config.getKerning(last_c, c);

    const Letter* letter = this->font_config.getLetter(c);
    if (letter == nullptr) {
      continue;
    }

    this->sprites[ i ] = Sprite::create(
      letter->texture_name,
      letter->texture_rect,
      { 
        cursor.x + letter->xoffset + kerning,
        cursor.y + letter->yoffset
      },
      this->color
    );

    last_c = c;
    cursor.x += letter->xadvance;
  }
}

bool TextBox::isValid(uint32_t c) const {
  const Letter* letter = this->font_config.getLetter(c);
  return letter != nullptr;
}

void TextBox::draw(SpriteBatch& sprite_batch) {
  assert(this->font_config.isValidated());
  for (uint32_t i = 0; i < this->tool_text.size(); i++) {
    sprite_batch.draw(this->sprites[ i ].get());
  }
}

void TextBox::clear() {
  assert(this->font_config.isValidated());

  this->setText(L"", this->color);
}
}

