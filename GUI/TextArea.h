#ifndef _TEXT_AREA_H_
#define _TEXT_AREA_H_

#include <iostream>
#include <Herzo/Common/DataType.h>
#include <Herzo/GUI/FontConfig.h>
#include <Herzo/Render/SpriteBatch.h>
#include <Herzo/Render/Sprite.h>
#include <memory>
#include <vector>
#include <assert.h>

namespace Herzo {
enum class TextAreaAlign {
    LEFT,
    MIDDLE
};

class TextArea {
public:
    static std::unique_ptr<TextArea> create(const FontConfig& font_config, const iRect& box);

    void setPosition(const iVec& position);
    void setText(
        std::wstring text,
        uint32_t color,
        TextAreaAlign align = TextAreaAlign::LEFT
    );
    bool isValid(uint32_t c) const;
    void draw(SpriteBatch& sprite_batch);

    iRect getBounds();

    void clear();

private:
    TextArea();
    FontConfig font_config;
    TextAreaAlign current_align = TextAreaAlign::LEFT;
    iRect box;
    // TODO, create a selfcontained class instead of Sprite pointers
    std::vector<std::unique_ptr<Sprite> > sprites;
    iVec position;
    int32_t color;
    std::vector<uint32_t> tool_text;
    std::vector<uint32_t> tool_word_size;
    std::vector<std::vector<int32_t> > to_align;
    std::vector<int32_t> to_align_acc_width;
};
}

#endif
