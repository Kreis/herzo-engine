#ifndef _TEXT_BOX_H_
#define _TEXT_BOX_H_

#include <iostream>
#include <Herzo/Common/DataType.h>
#include <Herzo/GUI/FontConfig.h>
#include <Herzo/Render/SpriteBatch.h>
#include <Herzo/Render/Sprite.h>
#include <memory>
#include <algorithm>
#include <assert.h>

namespace Herzo {
class TextBox {
public:
  static std::unique_ptr<TextBox> create(
    const FontConfig& font_config,
    const iRect& box);

  // x,y = left down corner, height ignored
  void setBox(const iRect& box);
  void setText(std::wstring text, int32_t color);
  bool isValid(uint32_t c) const;
  void draw(SpriteBatch& sprite_batch);

  void clear();

private:
  TextBox();
  FontConfig font_config;
  // TODO, replace for a selfcontained class without vector of pointers
  std::vector<std::unique_ptr<Sprite> > sprites;
  iRect box;
  int32_t color;
  std::vector<uint32_t> tool_text;
};
}

#endif
