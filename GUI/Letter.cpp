#include "Letter.h"

namespace Herzo {
Letter::Letter(
    char letter,
    int x,
    int y,
    int width,
    int height,
    int xoffset,
    int yoffset,
    int xadvance,
    std::string texture_name
    ) {

	this->letter = letter;
	this->texture_rect.x = x;
	this->texture_rect.y = y;
	this->texture_rect.w = width;
	this->texture_rect.h = height;
	this->xoffset = xoffset;
	this->yoffset = yoffset;
	this->xadvance = xadvance;
	this->texture_name = texture_name;
}
}
