#ifndef _LETTER_H_
#define _LETTER_H_

#include <iostream>
#include <memory>
#include <Herzo/Common/DataType.h>

namespace Herzo {
class Letter {
public:
	Letter(
        char letter,
        int x,
        int y,
        int width,
        int height,
        int xoffset,
        int yoffset,
        int xadvance,
        std::string texture_name
    );

	char letter;
	iRect texture_rect;
	int xoffset;
	int yoffset;
	int xadvance;
	std::string texture_name;
};
}

#endif
