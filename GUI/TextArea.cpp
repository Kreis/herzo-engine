#include "TextArea.h"

namespace Herzo {
TextArea::TextArea() {
  this->sprites = std::vector<std::unique_ptr<Sprite> >(1000);
}

std::unique_ptr<TextArea> TextArea::create(
    const FontConfig& font_config,
    const iRect& box) {

  std::unique_ptr<TextArea> text_area =
    std::unique_ptr<TextArea>(new TextArea());
  text_area->font_config = font_config;
  text_area->box = box;

  return std::move(text_area);
}

void TextArea::setPosition(const iVec& position) {
    this->box.x = position.x;
    this->box.y = position.y;
    this->setText(
        std::wstring(this->tool_text.begin(), this->tool_text.end()),
        this->color,
        this->current_align
    );
}

void TextArea::setText(
    std::wstring text,
    uint32_t color,
    TextAreaAlign align
) {
    assert(this->font_config.isValidated());
    this->current_align = align;
    this->color = color;

    this->tool_text.clear();
    this->tool_word_size.clear();
    for (const uint32_t c : text) {
        if ( ! this->isValid(c)) {
            continue;
        }
        this->tool_text.push_back(c);
        this->tool_word_size.push_back(0);
    }

    // setting size of words
    for (uint32_t i = 0; i < this->tool_text.size(); i++) {
        const Letter* letter = this->font_config.getLetter(
            this->tool_text[ i ]
        );
        assert(letter != nullptr);

        if (letter->letter == ' ') {
            continue;
        }

        // first letter of the word
        uint32_t current_width = 0;
        uint32_t j;
        for (j = i; j < this->tool_text.size(); j++) {
            const Letter* letter_2 = this->font_config.getLetter(
                this->tool_text[ j ]
            );
            assert(letter_2 != nullptr);

            // at the end of the word, break
            if (letter_2->letter == ' ') {
                break;
            }

            current_width += letter_2->xadvance;
        }

        this->tool_word_size[ i ] = current_width;
        current_width = 0;
        i = j;
        continue;
    }

    this->to_align.clear();
    this->to_align_acc_width.clear();

    iVec cursor;
    cursor.x = this->box.x;
    cursor.y = this->box.y + this->font_config.getLineHeight();
    uint32_t c = 0;
    uint32_t last_c;
    uint32_t acc_width = 0;
    bool new_row = true;
    // setting sprites
    for (uint32_t i = 0; i < this->tool_text.size(); i++) {
        last_c = c;
        c = this->tool_text[ i ];
        const Letter* letter = this->font_config.getLetter(c);
        assert(letter != nullptr);
        if (
            this->tool_word_size[ i ] > 0 &&
            acc_width + this->tool_word_size[ i ] > this->box.w
        ) {
            acc_width = 0;
            cursor.y += this->font_config.getLineHeight();
            cursor.x = this->box.x;
            new_row = true;
        }

        int8_t kerning = this->font_config.getKerning(last_c, c);
        this->sprites[ i ] = Sprite::create(
            letter->texture_name,
            letter->texture_rect,
            {
                cursor.x + letter->xoffset + kerning,
                cursor.y + letter->yoffset
            },
            this->color
        );

        acc_width += letter->xadvance;
        cursor.x += letter->xadvance;

        if (new_row) {
            this->to_align.push_back(std::vector<int32_t>());
            this->to_align_acc_width.push_back(0);
            new_row = false;
        }
        int32_t last_id = this->to_align.size() - 1;
        this->to_align[ last_id ].push_back(i);
        this->to_align_acc_width[ last_id ] = acc_width;
    }

    // Align
    if (align == TextAreaAlign::MIDDLE) {
        for (int32_t i = 0; i < this->to_align.size(); i++) {
            std::vector<int32_t>& row = this->to_align[ i ];
            int32_t offset = (this->box.w - this->to_align_acc_width[ i ]) / 2;
            for (int32_t j = 0; j < row.size(); j++) {
                Sprite* sprite = this->sprites[ this->to_align[ i ][ j ] ].get();
                const fVec& position = sprite->getPosition();
                sprite->setPosition(
                    { position.x + offset, position.y }
                );
            }
        }
    }

    // Adjust height each text input
    this->box.h = cursor.y - this->box.y;
}

bool TextArea::isValid(uint32_t c) const {
    const Letter* letter = this->font_config.getLetter(c);
    return letter != nullptr;
}

void TextArea::draw(SpriteBatch& sprite_batch) {
    assert(this->font_config.isValidated());

    for (uint32_t i = 0; i < this->tool_text.size(); i++) {
        sprite_batch.draw(this->sprites[ i ].get());
    }
}

iRect TextArea::getBounds() {
    return this->box;
}

void TextArea::clear() {
    assert(this->font_config.isValidated());
    this->setText(L"", 0 /* color */);
}
}

